using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class secretcollection : MonoBehaviour
{
    public bool secretFound;

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    private void Start()
    {
        //secretFound = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            secretFound = true;
            this.GetComponent<secretcollection>().enabled = false;
        }
    }

}
