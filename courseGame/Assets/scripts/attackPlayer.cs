using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class attackPlayer : MonoBehaviour
{

    private Animator anim;
    private bool isAttacking;
    public GameObject swordhitbox;
    int animLayer = 0;
    private float attackTimerSwing;
    private float startAttackTimerSwing;



    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
        isAttacking = false;
        swordhitbox.SetActive(false);
        attackTimerSwing = 0.0f;
        startAttackTimerSwing = 0.0f;


    }

   
    void Update()
    {
      
            

        if (Input.GetMouseButtonDown(0) && attackTimerSwing <= 0)
            {
                attackTimerSwing = 1.4f;
            startAttackTimerSwing = 1.8f;
            anim.SetBool("isAttackingSwing", true);

        }
        if (Input.GetMouseButtonDown(1) && attackTimerSwing <= 0)
        {
            attackTimerSwing = 1.4f;
            startAttackTimerSwing = 1.8f;
            anim.SetBool("isAttackingThrust", true);
        }
           attackTimerSwing -= Time.deltaTime;
        startAttackTimerSwing -= Time.deltaTime;

        if (startAttackTimerSwing > 0)
        {

            swordhitbox.SetActive(true);
        }
        else
        {

            swordhitbox.SetActive(false);
        }

        if (attackTimerSwing > 0)
        {
            
            //swordhitbox.SetActive(true);
            
        }

        else
        {
            anim.SetBool("isAttackingThrust", false);
            anim.SetBool("isAttackingSwing", false);
            //swordhitbox.SetActive(false);
        }

        

    }
}
