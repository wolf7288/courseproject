using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dontDestroy : MonoBehaviour
{
    public GameObject secret;
    private secretcollection secretcollected;

    public bool sec = false;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    private void Update()
    {
        if (secretcollected.secretFound == true)
        {
            sec = true;
        }
    }
}
