using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(CharacterController))]

public class PlayerController : MonoBehaviour
{

    //sorry for the mess
    public float speed = 5.5f;
    public float startDodgeTimer = 1.0f;
    private float dodgeTimer;
    public float gravity = 20.0f;
    public Transform playerCameraParent;
    public float lookSpeed = 2.0f;
    public float lookXLimit = 60.0f;
    private Animation anim;
    private bool isDodging;

    private bool playerDead = false;

    public Image healthbar;
    public Text healthCounter;
    public Text HealsRemaining;
    public Image secret;
    

    public ParticleSystem dodgePart;

    public int maxHealth = 25;
    public int health;
    public int damage = 1;

    public int heals;

    private float healthPerc = 1.0f;

    private Component Ccollider;

    CharacterController characterController;
    Vector3 moveDirection = Vector3.zero;
    Vector2 rotation = Vector2.zero;

    [HideInInspector]
    public bool canMove = true;

    public GameObject secretChest;
    private secretcollection sec;
    

    private void Awake()
    {
        //DontDestroyOnLoad(this);
    }

    void Start()
    {
        characterController = GetComponent<CharacterController>();
        rotation.y = transform.eulerAngles.y;
        dodgePart.Stop();
        isDodging = false;
        health = maxHealth;
        sec = secretChest.GetComponent<secretcollection>();
        secret.fillAmount = 0;
        
    }

    void Update()
    {
        if(Time.timeScale == 0)
        {
            canMove = false;
        }
        else
        {
            canMove = true;
        }

        if (sec.secretFound == true)
        {
            secret.fillAmount = 1.0f;

        }

        healthPerc = (float)health / (float)maxHealth;

        healthbar.fillAmount = healthPerc;

        healthCounter.text = health.ToString();

        HealsRemaining.text = heals.ToString();

        if (characterController.isGrounded)
        {

            Vector3 forward = transform.TransformDirection(Vector3.forward);
            Vector3 right = transform.TransformDirection(Vector3.right);
            float curSpeedX = canMove ? speed * Input.GetAxis("Vertical") : 0;
            float curSpeedY = canMove ? speed * Input.GetAxis("Horizontal") : 0;
            moveDirection = (forward * curSpeedX) + (right * curSpeedY);

            if (Input.GetButton("Jump") && canMove && dodgeTimer <= 1)
            {
                dodge();
            }
        }

        void dodge()
        {
            GetComponent<CapsuleCollider>().enabled = false;
            dodgeTimer = startDodgeTimer;
            dodgePart.Play();
            isDodging = true;
        }

        dodgeTimer -= Time.deltaTime;

        if (dodgeTimer <= 0)
        {
            GetComponent<CapsuleCollider>().enabled = true;
            isDodging = false;
        }

        moveDirection.y -= gravity * Time.deltaTime;


        characterController.Move(moveDirection * Time.deltaTime);


        if (canMove)
        {
            rotation.y += Input.GetAxis("Mouse X") * lookSpeed;
            rotation.x += -Input.GetAxis("Mouse Y") * lookSpeed;
            rotation.x = Mathf.Clamp(rotation.x, -lookXLimit, lookXLimit);
            playerCameraParent.localRotation = Quaternion.Euler(rotation.x, 0, 0);
            transform.eulerAngles = new Vector2(0, rotation.y);
        }

        if (Input.GetKeyDown(KeyCode.R) && heals > 0)
        {
            Heal();

        }
    }

    

     void Heal()
    {
        heals = heals - 1;
        health = maxHealth;

    }
    void OnTriggerEnter(Collider other )

    {



        if (isDodging == false && other.tag == "meleeEnemy" )
        {
            health -= damage;

            if (health <= 0)
            {
                playerDied();
            }
        }
        
    }

    void playerDied()
    {
        Destroy(gameObject);
        canMove = false;
        playerDead = true;
        SceneManager.LoadScene("YouDied");
    }
}