using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public GameObject main;
    public GameObject options;
    public GameObject tutorial;

    private void Awake()
    {
        main.SetActive(true);
        options.SetActive(false);
        tutorial.SetActive(false);
    }

    private void Update()
    {
        if (Input.anyKey)
        {
            Cursor.lockState = CursorLockMode.None;
        }
    }
    
    public void PlayGame()
    {
        SceneManager.LoadScene("level1");
    }
    public void Options()
    {
        main.SetActive(false);
        options.SetActive(true);

    }
    public void Tutorial()
    {
        main.SetActive(false);
        tutorial.SetActive(true);
    }
    public void Back()
    {
        main.SetActive(true);
        options.SetActive(false);
        tutorial.SetActive(false);

    }
    public void Quit()
    {
        Application.Quit();
    }
}
