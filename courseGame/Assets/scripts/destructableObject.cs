using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destructableObject : MonoBehaviour
{
    public GameObject destroyable;

    public int objectHealth;
    public int objectDamage;

    private void Start()
    {
        destroyable.SetActive(true);

    }
    private void Update()
    {
        //if (Input.GetMouseButtonDown(0))
        //{
        //    Destroy(gameObject);
        //}

    }

    private void OnTriggerEnter(Collider  other )
    
        {

        if (other.tag == "melee")
        {

            objectHealth -= objectDamage;

            if (objectHealth <= 0)
            {
                Destroy(destroyable);
            }
        }
        
    }

}
