using UnityEngine;
using UnityEngine.AI;


public class FollowScript : MonoBehaviour
{
    public float health = 2;
    public float damage = 1;
    public NavMeshAgent agent;

    public Transform player;

    public LayerMask whatIsGround, whatIsPlayer;

    private float attackTimer;
    private bool Attacking;


    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkPointRange;

    public float timeBetweenAttacks;
    bool alreadyAttacked;

    public float sightRange, attackRange;
    public bool playerInSightRange, playerInAttackRange;
    public Animator anim;
    private void Awake()
    {
        
        player = GameObject.Find("Player").transform;
        agent = GetComponent<NavMeshAgent>();
        anim = gameObject.GetComponent<Animator>();
    }

    private void Start()
    {
        attackTimer = 0f;
        Attacking = false;
    }

    private void Update()
    {
        //if (attackTimer < 0)
        //{
        //    ResetAttack();
        //}
        attackTimer -= Time.deltaTime;
    }

    private void FixedUpdate()
    {
        playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
        playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);

        if (!playerInSightRange && !playerInAttackRange) Patroling();
        if (playerInSightRange && !playerInAttackRange) ChasePlayer();
        if (playerInSightRange && playerInAttackRange) AttackPlayer();
    }

    private void Patroling()
    {
        if (!walkPointSet) SearchWalkPoint();

        if (walkPointSet)
            agent.SetDestination(walkPoint);

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        if (distanceToWalkPoint.magnitude < 1f)
            walkPointSet = false;
    }

    private void SearchWalkPoint()
    {
        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomX = Random.Range(-walkPointRange, walkPointRange);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        if (Physics.Raycast(walkPoint, -transform.up, 2f, whatIsGround))
            walkPointSet = true;
    }

    private void ChasePlayer()
    {
        agent.SetDestination(player.position);

    }

    private void AttackPlayer()
    {
        agent.SetDestination(transform.position);

        transform.LookAt(player);

        if (!alreadyAttacked && attackTimer <=0)
        {


            anim.SetBool("enemyAttacking", true);
            attackTimer = timeBetweenAttacks;

            alreadyAttacked = true;

            
            Invoke(nameof(ResetAttack), timeBetweenAttacks);
        }

    }





    private void ResetAttack()
    {
        
        anim.SetBool("enemyAttacking", false);
        alreadyAttacked = false;
        
        
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.tag == "melee")
        {
            health -= damage;

            if (health <= 0)
            {
                DestroyEnemy();
            }
        }
    }

    private void DestroyEnemy()
    {
        Destroy(gameObject);
    }

    

}