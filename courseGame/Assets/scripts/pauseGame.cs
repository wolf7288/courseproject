using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class pauseGame : MonoBehaviour
{
    private bool gamePaused = false;
    public GameObject PauseMenu;
    public GameObject MainPause;
    public GameObject Options;

    private void Start()
    {
        PauseMenu.SetActive(false);
        Options.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            gamePaused = !gamePaused;
            GamePaused();
        }
        
    }

    private void GamePaused()
    {
        if (gamePaused == true)
        {
            Time.timeScale = 0f;
            PauseMenu.SetActive(true);
            Cursor.lockState = CursorLockMode.None;
        }
        else if (gamePaused == false)
        {
            Time.timeScale = 1.0f;
            PauseMenu.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
    public void Resume()
    {
        gamePaused = false;
        GamePaused();
    }
    public void OptionsMenu()
    {
        Options.SetActive(true);
        MainPause.SetActive(false);
    }
    public void Back()
    {
        Options.SetActive(false);
        MainPause.SetActive(true);
    }
    public void Menu()
    {
        SceneManager.LoadScene(1);
    }
}
